from pytrends.request import TrendReq


def getTrends(list_words='love', time_interval='now 1-H', format_return="" ):
    td = TrendReq()
    td.build_payload(list_words, cat=0, timeframe=time_interval)
    td = td.interest_over_time()
    # return tg.to_json() if format_return == 'json' else tg
    return td.to_json() if format_return == "json" else td


from flask import Flask, jsonify
app = Flask(__name__)

@app.route('/')
def index():
    data = getTrends(['banana', 'tomato'], format_return="json")
    return jsonify(data) 

if __name__ == '__main__':
    app.run(debug=True)
