---
title: css print
slug: css-print
---

![](http://workshops.luuse.fun/_fig-festival/img/0.png)
<small>Image de Raphael Bastide</small>

« Le même contenu a deux formes différentes mais complémentaires, produites par le même outil : un navigateur web, connecté à internet d’un côté, et capable de générer un PDF de l’autre. Il n’y a pas la nécessité d’utiliser plusieurs logiciels, d’effectuer des opérations complexes pour passer d’une version “numérique” à un fichier imprimable, mais simplement du logiciel le plus utilisé : un navigateur web. »
<small>
	[Antoine Fauchié](http://strabic.fr/Workshop-PrePostPrint)
</small>

![](http://workshops.luuse.fun/_fig-festival/img/00.png)
<small>Image de Loraine Furter</small>

### @media print


Dans le fichier `css`.
```css
@media print{
	@page{
		size: 21cm 29.7cm;
	}
	@page :left {
		margin-left: 3cm;
	}
	@page :right {
		margin-left: 4cm;
	}
	body {
		color: red;
	}
}
```

### Les sauts de page   

Les `page-break` servent à définir des actions de saut de page avant ou après un type d'élement.   

```
page-break-after  : auto | always | avoid | left | right
page-break-before : auto | always | avoid | left | right
```
