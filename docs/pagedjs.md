---
title: pagedJs 
slug: pagedJs 
---


Paged.js est une bibliothèque JavaScript open-source permettant de concevoir un contenu paginé dans un navigateur et de générer des documents PDF destinés à l’impression, en utilisant les langages du web (HTML, CSS et JavaScript).

Elle est aujourd’hui l’un des outils les plus performants pour le web to print, particulièrement lorsque l’on veut concevoir des documents multi-pages.

La documentation officielle -> [ici](https://www.pagedjs.org/documentation/)   

Télécharger le dossier source -> [ici](https://antoine-gelgon.gitlab.io/w2p-cambre/exemples/pagedjs.zip)

## Server Web

Paged.js a besoin d’un server web pour être exécuté.
Dans vsCode nous pouvons installer le plugin [live server](https://ritwickdey.github.io/vscode-live-server/)

## Les zones de marge

![](https://pagedjs.org/images/margin-boxes.png)

Exemple de syntaxe css de titre courant sur la page de droite en haut à gauche.

```
@page: right {
  @top-right {
    content: "My title";
  }
}
```

Documentation officielle -> [ici](https://pagedjs.org/documentation/7-generated-content-in-margin-boxes/) 


### Format document 

```
@page {
  size: 148mm 210mm;
  margin: 1cm;
}

@page :left{
  margin: 33mm 23mm 65mm 45mm; 
}

@page :right{
  margin: 33mm 45mm 65mm 23mm;
}
```

### Crop marks

```
@page {
  ...
  marks: crop;
  bleed: 4mm;
}
```

### Folio

```
@page :left{ 
  @bottom-left {
    content: counter(page);
  }
}
@page :right{ 
  @bottom-right {
    content: counter(page);
  }
}
``` 


## Exemple de requets d'API avec PagedJs


```
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>
	<link href="css/interface.css" rel="stylesheet" type="text/css" />
	<link href="css/style.css" rel="stylesheet" type="text/css" />
	<style>
	</style>
</head>
<body>
	<section id="content">
	</section>
	<script>
		// FONCTION QUI STOP PAGEDJS
		window.PagedConfig = {
			auto: false,
			after: (flow) => { console.log("after", flow) },
		};

		// FONCTION POUR RÉCUPERER LES INFO DE L'API EN JSON
		async function getApi() {
			var names = []
			var response = await fetch("https://randomuser.me/api")
			var data = await response.json()
			return data 
		}

		// NOUS FAISONS FONCTIONNER L'API
		getApi().then(data => {
			console.log(data)
			window.PagedPolyfill.preview();
		})
	</script>
	<script src="https://unpkg.com/pagedjs/dist/paged.polyfill.js"></script>
</body>
</html>

```

