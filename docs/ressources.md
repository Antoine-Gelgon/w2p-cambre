---
title: ressources
slug: ressources
---

### WebToPrint - Documentation
* designing for print with css -> [https://www.smashingmagazine.com/2015/01/designing-for-print-with-css/](https://www.smashingmagazine.com/2015/01/designing-for-print-with-css/)

### WebToPrint - Projects 

* PrePostPrint → [https://prepostprint.org/doku.php/en/introduction](https://prepostprint.org/doku.php/en/introduction)  
* Post Digital Publishing Archives → [http://p-dpa.net/](http://p-dpa.net/)  
* Library of the Printed Web → [http://libraryoftheprintedweb.tumblr.com/](http://libraryoftheprintedweb.tumblr.com/)  
* Center for Future Publishing [https://centerforfuturepublishing.org/](https://centerforfuturepublishing.org/)  
* Piet Zwart institute - XPub - [https://project.xpub.nl/](https://project.xpub.nl/)  
* Eric Schrijver - Hybrid Publishing Back To The Future Publishing Theses at the KABK [http://kabk.github.io/govt-theses-15/](http://kabk.github.io/govt-theses-15/)
* The Death of the Authors - [https://www.books.constantvzw.org/fr/home/death_of_the_authors](https://www.books.constantvzw.org/fr/home/death_of_the_authors)
* Graphic Design in the Post-Digital Age - [https://www.postdigitalgraphicdesign.com/](https://www.postdigitalgraphicdesign.com/)
* Radical Web -  [http://radicalweb.design/en](http://radicalweb.design/en)
* What IsDat - [https://what.isdat.fr/](https://what.isdat.fr/)
* center for future publishing - [https://centerforfuturepublishing.wordpress.com/](https://centerforfuturepublishing.wordpress.com/)

### WebToPrint — Tools  

* css print - [https://www.print-css.rocks/](https://www.print-css.rocks/)
* Html2print - [http://osp.kitchen/tools/html2print/](http://osp.kitchen/tools/html2print/)
* paged.js - [https://www.pagedmedia.org/paged-js/](https://www.pagedmedia.org/paged-js/)
* Letter - [https://github.com/bastianallgeier/letter](https://github.com/bastianallgeier/letter)
* Even - [https://xxyxyz.org/even/](https://xxyxyz.org/even/)
* Libris - [https://github.com/bachy/libriis](https://github.com/bachy/libriis)
* Bindery.js - [https://bindery.info/](https://bindery.info/)
* weasyprint - [https://weasyprint.org/](https://weasyprint.org/)
* Basiljs - [http://basiljs.ch/about/](http://basiljs.ch/about/)
* Yah2p - [https://gitlab.com/Luuse/Luuse.tools/web2paper](https://gitlab.com/Luuse/Luuse.tools/web2paper)
* Ethertoff - [http://osp.kitchen/tools/ethertoff/](http://osp.kitchen/tools/ethertoff/)
* Print With CSS - [https://www.smashingmagazine.com/2015/01/designing-for-print-with-css/](https://www.smashingmagazine.com/2015/01/designing-for-print-with-css/)

### WebToPrint - Enthousiastics

* OSP → [http://osp.kitchen/](http://osp.kitchen/)
* La Villa Hermosa → [http://lavillahermosa.com/](http://lavillahermosa.com/)
* Bonjour Monde → [http://bonjourmonde.net/](http://bonjourmonde.net/)
* Loraine Furter → [https://lorainefurter.net/en](https://lorainefurter.net/en)
* Eric Schrijver → [https://ericschrijver.nl/](https://ericschrijver.nl/)
* g.u.i. → [https://www.g-u-i.net/en](https://www.g-u-i.net/en)
* Sarah Garcin → [http://www.sarahgarcin.com/](http://www.sarahgarcin.com/)
* Raphaël Bastide → [https://raphaelbastide.com/](https://raphaelbastide.com/)
* Louise Druhle → [https://www.louisedrulhe.fr/](https://www.louisedrulhe.fr/)
* Lena Robin→ [http://www.lenarobin.xyz/](http://www.lenarobin.xyz/) 
* Marianne Plano -> [https://marianneplano.net/](https://marianneplano.net/)
* Julie Blanc -> [https://julie-blanc.fr/](https://julie-blanc.fr/)
* Lucile Haute -> [http://www.lucilehaute.com/](http://www.lucilehaute.com/)
* Julien Bidoret -> [https://accentgrave.net/](https://accentgrave.net/)


### Fonts libres de droits

* Use and Modify → [https://usemodify.com/](https://usemodify.com/)
* GoogleFonts →[https://fonts.google.com/](https://fonts.google.com/)
* Velvetyne → [https://www.velvetyne.fr/](https://www.velvetyne.fr/ )
* Font Squirrel → [https://www.fontsquirrel.com/](https://www.fontsquirrel.com/)
* Font Library → [https://fontlibrary.org/en]( https://fontlibrary.org/en)
* By Womxn → [http://design-research.be/by-womxn/](http://design-research.be/by-womxn/)
* OSP Foundry → [http://osp.kitchen/foundry/](http://osp.kitchen/foundry/)
* Sébastien San Filipo → [http://love-letters.be/foundry.html](http://love-letters.be/foundry.html)
* Omnibus Type → [https://www.omnibus-type.com/](https://www.omnibus-type.com/)
* Temporary State → [http://typefaces.temporarystate.net/preview/](http://typefaces.temporarystate.net/preview/)
* Classique free font -> [https://gitlab.com/Antoine-Gelgon/classic-fonts-libre](https://gitlab.com/Antoine-Gelgon/classic-fonts-libre)

Lien vers le drive -> [ici](https://drive.google.com/drive/u/3/folders/1QEUBq4qpxLHy3md4lnlf2IBLMNUPNnCk) 
