---
title: Les API
slug: api

---
> «En informatique, une interface de programmation d’application ou interface de programmation applicative2,3,4 (souvent désignée par le terme API pour Application Programming Interface) est un ensemble normalisé de classes, de méthodes, de fonctions et de constantes qui sert de façade par laquelle un logiciel offre des services à d'autres logiciels. Elle est offerte par une bibliothèque logicielle ou un service web, le plus souvent accompagnée d'une description qui spécifie comment des programmes consommateurs peuvent se servir des fonctionnalités du programme fournisseur.»

[https://fr.wikipedia.org/wiki/Interface_de_programmation](https://fr.wikipedia.org/wiki/Interface_de_programmation) 

## Exemples API

Vous pouvez trouver tout un tas d'API gratuit trié par domaine ici -> [github.com/public-apis/public-apis](https://github.com/public-apis/public-apis)

### randomuser
Lien api : [https://randomuser.me/](https://randomuser.me/)  
Exemple réponse api : [https://randomuser.me/api/](https://randomuser.me/api/)  
Exemple d'application : [link](https://antoine-gelgon.gitlab.io/w2p-cambre/exemples/api/login-random/)  
Code source de l'exemple : [link](https://gitlab.com/Antoine-Gelgon/w2p-cambre/-/blob/main/theme/exemples/api/login-random/index.html) 

### open-meteo 
Lien api : [https://open-meteo.com/en](https://open-meteo.com/en)  
Documentation api : : [https://open-meteo.com/en/docs](https://open-meteo.com/en/docs)  
Exemple réponse api : [link](https://api.open-meteo.com/v1/forecast?latitude=50.8371&longitude=4.3676&hourly=temperature_2m)  
Exemple d'application : [link](https://antoine-gelgon.gitlab.io/w2p-cambre/exemples/api/meteo/)  
Code source de l'exemple : [link](https://gitlab.com/Antoine-Gelgon/w2p-cambre/-/blob/main/theme/exemples/api/meteo/index.html) 


### pixabay
Lien api : [https://pixabay.com/](https://pixabay.com/)  
Documentation api : [https://pixabay.com/api/docs/  ](https://pixabay.com/api/docs/)  
Exemple réponse api : [link](https://pixabay.com/api/?key=30714257-d06ed4b49a448ef56bff76737&q=cat+licking&image_type=photo&per_page=10)  
Exemple d'application : [link 1 ](https://antoine-gelgon.gitlab.io/w2p-cambre/exemples/api/pixabay/), [link 2 ](https://antoine-gelgon.gitlab.io/w2p-cambre/exemples/api/cat/)   
Code source de l'exemple : [link 1](https://gitlab.com/Antoine-Gelgon/w2p-cambre/-/blob/main/theme/exemples/api/pixabay/index.html), [link 2](https://gitlab.com/Antoine-Gelgon/w2p-cambre/-/blob/main/theme/exemples/api/cat/index.html)   

### gutendex
Lien api : [https://gutendex.com](https://gutendex.com)  
Exemple réponse api : [link](http://gutendex.com/books?author_year_start=1800&author_year_end=1899)  
Exemple d'application : [link](https://antoine-gelgon.gitlab.io/w2p-cambre/exemples/api/gutendex/)
Code source de l'exemple : [link 1](https://gitlab.com/Antoine-Gelgon/w2p-cambre/-/tree/main/theme/exemples/api/gutendex)

### Windy 
Lien api : [https://www.windy.com](https://www.windy.com)
Documentation api : [https://api.windy.com/webcams/docs](https://api.windy.com/webcams/docs)  
Exemple réponse api : [link](https://api.windy.com/api/webcams/v2/list/country=BE/category=city/?key=VPrBStaM9T4e9cZ2XEAKdBgaClSJelTc&show=webcams:image,location;categories)  
Exemple d'application : [link](https://antoine-gelgon.gitlab.io/w2p-cambre/exemples/api/windy/)   
Code source de l'exemple : [link](https://gitlab.com/Antoine-Gelgon/w2p-cambre/-/blob/main/theme/exemples/api/windy/index.html)  

### Wikipedia
Lien api : [https://pixabay.com/](https://pixabay.com/)  
Documentation api : [https://github.com/mudroljub/wikipedia-api-docs](https://github.com/mudroljub/wikipedia-api-docs)  
Exemple réponse api : [link](https://fr.wikipedia.org/w/api.php?action=query&titles=Bruxelles&prop=pageimages|extracts&origin=*&format=json&pithumbsize=400)  
Exemple d'application : [link](https://antoine-gelgon.gitlab.io/w2p-cambre/exemples/api/wikipedia/)
Code source de l'exemple : [link](https://gitlab.com/Antoine-Gelgon/w2p-cambre/-/blob/main/theme/exemples/api/wikipedia/index.html)

## Requête javascript `fetch`

```
async function getAPI() {
	let response = await fetch(
			"URL DE MA REQUÊTE"
			);
	let data = await response.json();
	return data;
}

getAPI().then(data => {
	console.log(data)
})
```
