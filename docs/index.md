---
title: Introduction 
slug: intro
---

Dans ce cours nous tenterons de faire de l’édition imprimable avec des langages web, méthode souvent appelé web-to-print. Nous essayerons de comprendre ce qui est intrinsèque au web et plus largement à internet, la donnée. Entrevoir ce que sont les bases de données et les protocoles d'échange à travers un type d'objet, les API. En tant que designers et artistes que pouvons-nous et qu’allons-nous faire de ces données? Quel regard, quelle hiérarchie, quelle critique, allons nous cristalliser aux travers ces objets imprimés?
