---
title: CSS REGIONS (RIP)
slug: css-regions 
---

La fonctionnalité CSS Regions permet d'implémenter des conceptions complexes de style magazine dans lesquelles le contenu circule à travers des éléments de mise en page librement positionnés. Il vous permet de faire circuler dynamiquement le contenu d'un élément de mise en page à un autre, mais ne spécifie pas comment ces éléments sont présentés.

![](https://web-dev.imgix.net/image/T4FyVKpzu4WKF1kBNvXepbi08t52/GB9RxLwRmIl4Da8xcG9i.png)

[css regions polyfill](https://github.com/adobe-webplatform/css-regions-polyfill)

<!-- ### CSS REGION AVEC POLYFILL

Nos flux.
```
<div class="page"></div>
<div class="page"></div>
```

Notre contenu.
```
<div id="content" lang="fr" >
	Consectetur error suscipit ... 
</div>
```

Le css-regions qui va injecter le contenu dans le flux.
```
		<style>
				#content{
						-regions-flow-into: myFlow;
				}
				.page {
						-regions-flow-from: myFlow;
				}
		</style>

```


[lien du zip](https://antoine.luuse.fun/files/web-to-print-survival-kit.zip)-->
